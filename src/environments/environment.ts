// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBpmi3H-Gs4rPm1X41zadn0OXbm_ecaAl0",
    authDomain: "loginform-8e84d.firebaseapp.com",
    projectId: "loginform-8e84d",
    storageBucket: "loginform-8e84d.appspot.com",
    messagingSenderId: "422287047928",
    appId: "1:422287047928:web:3ceaf5d623df7d270fcf0d",
    measurementId: "G-GDVLNL4N61"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
