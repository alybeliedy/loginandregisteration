import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  userData:any;
  constructor(
    public afStore:AngularFirestore,
    public ngFireAuth:AngularFireAuth,
    public router:Router

  ) {
    this.ngFireAuth.authState.subscribe(user=>{
      if(user){
        this.userData=user;
        localStorage.setItem('user',JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user'))
      }else{
        localStorage.setItem('user',null);
        JSON.parse(localStorage.getItem('user'))
      }
    })
   }

   signin(email:string,password:string){
     return this.ngFireAuth.signInWithEmailAndPassword(email,password)
   }

   registerUser(email:string,password:string){
    return this.ngFireAuth.createUserWithEmailAndPassword(email,password)
  }
  //check if there user exist or not
  get isLoggedIn(): boolean{
    //if there user exist in locatStorage
   const user=JSON.parse(localStorage.getItem('user'))
   if (user!==null) {
    return true;
   } else {
     return false;
   }
  }
  //set user data in firestore
  setUserData(user:any){
    const userRef:AngularFirestoreDocument<any>=this.afStore.doc(`users/${user.uid}`)
    const userData:User={
      uid:user.uid,
      name:user.name,
      email:user.email,
      mobile:user.mobile,
      password:user.password,

    }
    return userRef.set(userData,{
      merge:true
    })
  }

  signout()
  {
    this.ngFireAuth.signOut()
    localStorage.removeItem('user')
    localStorage.removeItem('uid')
    this.router.navigate(['login'])
  }
}
