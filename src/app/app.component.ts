import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { AuthenticationService } from './shared/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private platForm:Platform,
    public authService:AuthenticationService,
    public router:Router
  ) {
    this.initializeApp();
  }
  initializeApp(){
    this.platForm.ready()
    if (this.authService.isLoggedIn) {
      this.router.navigate(['/mainscreen']);
    } else {
      this.router.navigate(['/login']);
    }
  }
}
