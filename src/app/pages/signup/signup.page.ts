import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User';
import { AuthenticationService } from 'src/app/shared/authentication.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  constructor(
    public authService:AuthenticationService,
    public router:Router,

  ) { }

  ngOnInit() {
  }
  signup(name,mobile,email,password){
    this.authService.registerUser(email.value,password.value)
    .then((res)=>{
      const userData:User={
        uid:res.user.uid,
        name:name.value,
        email:res.user.email,
        mobile:mobile.value,
        password:password.value,

      }
      this.authService.setUserData(userData)
         localStorage.setItem('uid',res.user.uid)
         this.router.navigate(['/login']);

    }).catch(err=>console.log(err)
    )
  }
}
