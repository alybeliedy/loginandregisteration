import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'org.alybeliedy.loginform',
  appName: 'loginform',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
