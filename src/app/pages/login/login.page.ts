import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/shared/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    public authService:AuthenticationService,
    public router:Router,
  ) { }

  ngOnInit() {
  }
  gotoSignUp(){
    this.router.navigate(['/signup']);
  }
  forgetPassword(){
    this.router.navigate(['/forget-password']);
  }
  signIn(email,password){
    this.authService.signin(email.value,password.value)
    .then((res)=>{
         localStorage.setItem('uid',res.user.uid)
         this.router.navigate(['/mainscreen']);

    }).catch(err=>console.log(err)
    )
  }
}


